let musicContainer = document.querySelector(".musicContainer");
let playBtn = document.querySelector("#play");
let prevBtn = document.querySelector("#prev");
let nextBtn = document.querySelector("#next");
let audio = document.querySelector("#audio");
let progress = document.querySelector(".progress");
let progressContainer = document.querySelector(".progressContainer");
let title = document.querySelector("#title");
let cover = document.querySelector("#cover");

let songs = ["Piano","Despacito","Vivir"];
let songIndex = 1;
loadSong (songs[songIndex]);

function loadSong (song) {
    title.innerText = song;
    audio.src = `Musics/${song}.mp3`;
    cover.src = `images/${song}.jpeg`
}

function playSong () {
    musicContainer.classList.add("play");
    playBtn.querySelector("i.fas").classList.remove("fa-play");
    playBtn.querySelector("i.fas").classList.add("fa-pause");

    audio.play()
}

function pauseSong() {
    musicContainer.classList.remove("play");
    playBtn.querySelector("i.fas").classList.add("fa-play");
    playBtn.querySelector("i.fas").classList.remove("fa-pause");
    audio.pause()
};

function prevSong () {
    songIndex--;
    if(songIndex < 0) {
        songIndex = songs.length - 1;
    }
    loadSong(songs[songIndex]);
    playSong();
}

function nextSong () {
    songIndex++;
    if(songIndex > songs.length - 1) {
        songIndex = 0;
    }
    loadSong(songs[songIndex]);
    playSong();
}

function updateProgress (event) {
    let {duration, currentTime} = event.srcElement;
    let progressPercent = (currentTime / duration) * 100;
    progress.style.width = `${progressPercent}%`;
};

function setProgress (e) {
    const width = this.clientWidth;
    const clickX = e.offsetX;
    let duration = audio.duration;
    audio.currentTime = (clickX / width) * duration;
};

playBtn.addEventListener("click", () => {
    let isPlaying = musicContainer.classList.contains("play");
    if(isPlaying) {
        pauseSong();
    } else {
        playSong();
    }
});

prevBtn.addEventListener("click", () => prevSong());
nextBtn.addEventListener("click", () => nextSong());
audio.addEventListener("timeupdate", updateProgress);
progressContainer.addEventListener("click",setProgress);
audio.addEventListener("ended", nextSong);